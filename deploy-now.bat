echo ON
rmdir /S /Q export
call npm rm decentraland-ecs
call npm rm -g decentraland
echo ON
call npm i -g decentraland@3.2.0
call npm i decentraland-ecs@6.1.5
echo ON
copy scene-export.json scene.json /Y
call dcl build
call dcl export
copy now.json export\.
cd export
call now
cd ..
copy scene-deploy.json scene.json /Y
call npm rm decentraland-ecs
call npm rm -g decentraland
echo ON
call npm i -g decentraland
call npm i decentraland-ecs@latest
echo ON
