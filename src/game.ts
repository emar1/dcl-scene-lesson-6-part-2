/////////////////////////////////////////////////////////
// demo-BuilderHUD
// (c) 2019 Carl Fravel
/////////////////////////////////////////////////////////

//import {spawnGltfX, spawnEntity, spawnBoxX, spawnPlaneX} from './modules/SpawnerFunctions'
import {BuilderHUD} from './modules/BuilderHUD'
import { spawnEntity, spawnGltfX,spawnBoxX, spawnPlaneX} from './modules/SpawnerFunctions'

const scene = new Entity()
const transform = new Transform({
  position: new Vector3(0, 0, 0),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
scene.addComponentOrReplace(transform)
engine.addEntity(scene)

const floorBaseGrass_01 = spawnGltfX(new GLTFShape('models/FloorBaseGrass_01/FloorBaseGrass_01.glb'), 8,0,8, 0,0,0,  1,1,1)
floorBaseGrass_01.setParent(scene)

const cola = spawnGltfX(new GLTFShape('models/cola.glb'), 1.5,1,2.29,  0,0,0,  2,2,2)
cola.setParent(scene)

const radio = spawnGltfX(new GLTFShape('models/radio.glb'), 2.28,0.8,2.07,  0,-90,0,  0.5,0.5,0.5)
radio.setParent(scene)

const table = spawnGltfX(new GLTFShape('models/table.glb'), 2,0,2,  0,0,0,  1,1,1)
table.setParent(scene)

const Bishop = spawnGltfX(new GLTFShape('models/chess/Bishop.glb'), 8.98,1.42,-9.53,  0,0,0,  0.5,0.5,0.5)
Bishop.setParent(scene)


///////// Example of adding the BuilderHUD to your scene and attaching it to one existing scene entity.
const hud:BuilderHUD =  new BuilderHUD()
hud.attachToEntity(cola)
hud.attachToEntity(radio)
hud.attachToEntity(table)
hud.attachToEntity(Bishop)
hud.setDefaultParent(scene)